using System.ComponentModel.DataAnnotations;

namespace Front01.Models
{
    public class Usuario
    {
        [Key]
        public int id { get; set; }

        public string data { get; set; }

        [Required(ErrorMessage = "O campo username é obrigatório")]
        public string username { get; set; }

        [Required(ErrorMessage = "O campo password é obrigatório")]
        public string password { get; set; }
    }
}